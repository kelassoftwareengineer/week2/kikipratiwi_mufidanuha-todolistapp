# README

In first iteration of TodoListApp, we try to implement ["TDD"](https://hackernoon.com/introduction-to-test-driven-development-tdd-61a13bc92d92).

The repository contains features:
1. Add tasks to the list
2. Show all the tasks in a list